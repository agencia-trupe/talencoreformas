export default function Contato() {
    var envioContato = function(event) {
        event.preventDefault();

        var $form = $(this);

        if ($form.hasClass('sending')) return false;

        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                alert(data.message);
                if (data.status == 'success') $form[0].reset();
            },
            error: function(data) {
                alert('Ocorreu um erro. Tente novamente.');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-contato').on('submit', envioContato);
};
