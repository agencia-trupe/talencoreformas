import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Newsletter from './Newsletter';
import Contato from './Contato';
import Projetos from './Projetos';

AjaxSetup();
MobileToggle();
Newsletter();
Contato();
Projetos();

var onScroll = function(event) {
    var scrollPos = $(document).scrollTop();
    $('#nav-desktop a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr('href'));

        if (currLink.attr('href') == '#contato') {
            if ($('.newsletter-home').position().top <= scrollPos + 101 && !$('#nav-desktop a.active').length) {
                $('#nav-desktop a[href="#contato"]').addClass('active');
            }
        } else {
            var compensacao = currLink.attr('href') == '#projetos' ? 30 : 0;

            if (refElement.position().top <= scrollPos + 101 + compensacao && refElement.position().top + refElement.height() > scrollPos) {
                $('#nav-desktop a').removeClass('active');
                currLink.addClass('active');
            }
            else {
                currLink.removeClass('active');
            }
        }
    });
};

var scrollTo = function(section) {
    if (section === '#projetos') {
        var compensacao = $(window).width() > 767 ? 30 : 25;
        $('html, body').animate({
            scrollTop: $(section).offset().top - ($(window).width() > 1179 ? 100 : 0) - compensacao
        }, function() {
            $(document).on('scroll', onScroll);
        });
    } else {
        $('html, body').animate({
            scrollTop: $(section).offset().top - ($(window).width() > 1179 ? 100 : 0)
        }, function() {
            $(document).on('scroll', onScroll);
        });
    }
};

if ($('header').hasClass('fixed')) {
    $(document).on('scroll', onScroll);
    if (window.location.hash) {
        scrollTo(window.location.hash);
    }
}

$('.menu-scroll').on('click touchstart', function(event) {
    event.preventDefault();
    $(document).off('scroll');

    if (!$(this).hasClass('active')) $('.menu-scroll').removeClass('active');
    $(this).addClass('active');

    var section = $(this).attr('href');
    scrollTo(section);

    if ($(this).parent().is('#nav-mobile')) {
        $('#nav-mobile').slideToggle();
        $('#mobile-toggle').toggleClass('close');
    }
});

$('.banners').cycle({
    slides: '>.slide'
});

$('.fale-conosco span.telefone').tipso({
    background: '#E3B94D',
    color     : '#154A65',
    width     : 160,
    offsetX   : 12,
    position  : 'top-left'
});

$('header span.telefone').tipso({
    background: '#E3B94D',
    color     : '#154A65',
    width     : 160,
    offsetY   : -5,
    offsetX   : 12,
    position  : 'bottom-left'
});

$('.blog-ver-mais').click(function(event) {
    event.preventDefault();

    var $btn       = $(this),
        $container = $('.blog-posts');

    if ($btn.hasClass('loading')) return false;

    $btn.addClass('loading');

    $.get($btn.data('next'), function(data) {
        var posts = $(data.posts).hide();
        $container.append(posts);
        posts.fadeIn();

        $btn.removeClass('loading');

        if (data.nextPage) {
            $btn.data('next', data.nextPage);
        } else {
            $btn.fadeOut(function() {
                $this.remove();
            });
        }
    });
});

$('.depoimentos-slick').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    draggable: false,
    prevArrow: '<a href="#" class="slick-prev">Previous</a>',
    nextArrow: '<a href="#" class="slick-next">Next</a>',
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        },
    ]
});
