export default function Projetos() {
    $('.projetos-capas').cycle({
        next: '#next',
        prev: '#prev',
    }).on('cycle-before', function(event, option, current, next) {
        var $slide = $(next);

        $('.projetos-descricao h3').hide().html($slide.data('titulo')).fadeIn();
        $('.projetos-descricao p').hide().html($slide.data('descricao')).fadeIn();
        $('.projetos-descricao a').hide().data('id', $slide.data('id')).fadeIn();
    });

    $('.fancybox').fancybox({
        padding: 0,
    });

    $('.projetos-descricao a').click(function(event) {
        event.preventDefault();

        var el, id = $(this).data('id');
        if (id) {
            el = $('.fancybox[rel=' + id + ']:eq(0)');
            el.click();
        }
    });
};
