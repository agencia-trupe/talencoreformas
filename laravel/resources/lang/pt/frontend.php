<?php

return [
    'empresa'       => 'A EMPRESA',
    'o-que-fazemos' => 'O QUE FAZEMOS',
    'como-fazemos'  => 'COMO FAZEMOS',
    'blog'          => 'ESPAÇO TALENCO',
    'blog-destaque' => '<strong>ESPAÇO</strong>TALENCO',
    'projetos'      => [
        'titulo'     => 'PROJETOS',
        'saiba-mais' => 'VER FOTOS'
    ],
    'midia'         => 'MÍDIA',
    'newsletter'    => [
        'titulo'         => 'NEWSLETTER - CONTEÚDO EXCLUSIVO PARA VOCÊ',
        'nome'           => 'Nome',
        'inscrever'      => 'INSCREVER',
        'nome-required'  => 'preencha seu nome',
        'email-required' => 'insira um endereço de e-mail',
        'email-email'    => 'insira um endereço de e-mail válido',
        'email-unique'   => 'o e-mail inserido já está cadastrado',
        'sucesso'        => 'cadastro efetuado com sucesso!',
    ],
    'unidades' =>  [
        'titulo'  => 'UNIDADES DE NEGÓCIOS',
        'visitar' => 'VISITAR SITE',
    ],
    'contato' => [
        'titulo'            => 'CONTATO',
        'empresa'           => 'A Empresa',
        'o-que-fazemos'     => 'O que fazemos',
        'como-fazemos'      => 'Como fazemos',
        'blog'              => 'Espaço Talenco',
        'projetos'          => 'Projetos',
        'fale-conosco'      => 'FALE CONOSCO',
        'frase'             => 'Em caso de dúvidas ou orçamentos, entre em contato com a nossa equipe e em breve retornaremos seu contato.',
        'institucional'     => 'INSTITUCIONAL',
        'solucoes-reformas' => 'SOLUÇÕES EM REFORMAS',
        'telefone'          => 'Telefone',
        'mensagem'          => 'Mensagem',
        'enviar'            => 'Enviar',
        'sucesso'           => 'Mensagem enviada com sucesso!',
        'erro'              => 'Preencha todos os campos corretamente.',
    ],
    'copyright' => [
        'direitos' => 'Todos os direitos reservados',
        'criacao'  => 'Criação de sites'
    ],
    '404' => 'Página não encontrada',
];
