<?php

return [
    'empresa'       => 'ABOUT US',
    'o-que-fazemos' => 'WHAT WE DO',
    'como-fazemos'  => 'HOW WE WORK',
    'blog'          => 'BLOG',
    'blog-destaque' => '<strong>BLOG</strong>',
    'projetos'      => [
        'titulo'     => 'PORTFOLIO',
        'saiba-mais' => 'SEE PHOTOS'
    ],
    'midia'         => 'MEDIA',
    'newsletter'    => [
        'titulo'         => 'NEWSLETTER - EXCLUSIVE CONTENT FOR YOU',
        'nome'           => 'Name',
        'inscrever'      => 'SUBSCRIBE',
        'nome-required'  => 'fill in your name',
        'email-required' => 'enter an e-mail address',
        'email-email'    => 'enter a valid e-mail address',
        'email-unique'   => 'this e-mail is already registered',
        'sucesso'        => 'your registration was successful!',
    ],
    'unidades' =>  [
        'titulo'  => 'BUSINESS UNITS',
        'visitar' => 'VISIT SITE',
    ],
    'contato' => [
        'titulo'            => 'CONTACT',
        'empresa'           => 'About us',
        'o-que-fazemos'     => 'What we do',
        'como-fazemos'      => 'How we work',
        'blog'              => 'Blog',
        'projetos'          => 'Portfolio',
        'fale-conosco'      => 'TALK TO US',
        'frase'             => 'If you are interested in receiving a quote or meeting with a renovation expert please fill out our online form.',
        'institucional'     => 'INSTITUTIONAL',
        'solucoes-reformas' => 'REMODELING SOLUTIONS',
        'telefone'          => 'Phone',
        'mensagem'          => 'Message',
        'enviar'            => 'Send',
        'sucesso'           => 'Message sent successfully!',
        'erro'              => 'Fill all fields correctly.',
    ],
    'copyright' => [
        'direitos' => 'All rights reserved',
        'criacao'  => 'Sites'
    ],
    '404' => 'Page not found',
];
