    <header @if(isset($fixed)) class="fixed" @endif>
        <div class="center">
            <a class="logo">{{ $config->nome_do_site }}</a>
            <div class="icones">
                @foreach(['instagram', 'facebook', 'linkedin', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endif
                @endforeach
                @if($contato->telefone)
                <span class="telefone" data-tipso="{{ $contato->telefone }}">{{ $contato->telefone }}</span>
                @endif
            </div>
            <nav id="nav-desktop">
                <a href="#home" class="menu-scroll active">HOME</a>
                <a href="#empresa" class="menu-scroll">{{ trans('frontend.empresa') }}</a>
                <a href="#o-que-fazemos" class="menu-scroll">{{ trans('frontend.o-que-fazemos') }}</a>
                <a href="#como-fazemos" class="menu-scroll">{{ trans('frontend.como-fazemos') }}</a>
                <a href="#projetos" class="menu-scroll">{{ trans('frontend.projetos.titulo') }}</a>
                <a href="#midia" class="menu-scroll">{{ trans('frontend.midia') }}</a>
                <a href="#espaco-talenco" class="menu-scroll">{{ trans('frontend.blog') }}</a>
                <a href="#contato" class="menu-scroll">{{ trans('frontend.contato.titulo') }}</a>
            </nav>
            @if(app()->getLocale() == 'pt')
            <a href="{{ route('lang', 'en') }}" class="lang lang-en" title="English Version">en</a>
            @else
            <a href="{{ route('lang', 'pt') }}" class="lang lang-pt" title="Versão em Português">pt</a>
            @endif
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>
    <nav id="nav-mobile">
        <a href="#home" class="menu-scroll">HOME</a>
        <a href="#empresa" class="menu-scroll">{{ trans('frontend.empresa') }}</a>
        <a href="#o-que-fazemos" class="menu-scroll">{{ trans('frontend.o-que-fazemos') }}</a>
        <a href="#como-fazemos" class="menu-scroll">{{ trans('frontend.como-fazemos') }}</a>
        <a href="#projetos" class="menu-scroll">{{ trans('frontend.projetos.titulo') }}</a>
        <a href="#midia" class="menu-scroll">{{ trans('frontend.midia') }}</a>
        <a href="#espaco-talenco" class="menu-scroll">{{ trans('frontend.blog') }}</a>
        <a href="#contato" class="menu-scroll">{{ trans('frontend.contato.titulo') }}</a>
    </nav>
