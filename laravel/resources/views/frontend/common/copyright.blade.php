<div class="copyright">
    <div class="center">
        <p>© {{ date('Y') }} TalenCo Reformas - {{ trans('frontend.copyright.direitos') }}.</p>
        <p>
            <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.copyright.criacao') }}</a>:
            <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
        </p>
    </div>
</div>
