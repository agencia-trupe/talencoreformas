    <header @if(isset($fixed)) class="fixed" @endif>
        <div class="center">
            <a class="logo">{{ $config->nome_do_site }}</a>
            <div class="icones">
                @foreach(['instagram', 'facebook', 'linkedin', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endif
                @endforeach
                @if($contato->telefone)
                <span class="telefone" data-tipso="{{ $contato->telefone }}">{{ $contato->telefone }}</span>
                @endif
            </div>
            <nav id="nav-desktop">
                <a href="#home">HOME</a>
                <a href="#empresa">{{ trans('frontend.empresa') }}</a>
                <a href="#o-que-fazemos">{{ trans('frontend.o-que-fazemos') }}</a>
                <a href="#como-fazemos">{{ trans('frontend.como-fazemos') }}</a>
                <a href="#projetos">{{ trans('frontend.projetos.titulo') }}</a>
                <a href="#midia">{{ trans('frontend.midia') }}</a>
                <a class="active">{{ trans('frontend.blog') }}</a>
                <a href="#contato">{{ trans('frontend.contato.titulo') }}</a>
            </nav>
            @if(app()->getLocale() == 'pt')
            <a href="{{ route('lang', 'en') }}" class="lang lang-en" title="English Version">en</a>
            @else
            <a href="{{ route('lang', 'pt') }}" class="lang lang-pt" title="Versão em Português">pt</a>
            @endif
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>
    <nav id="nav-mobile">
        <a href="#home">HOME</a>
        <a href="#empresa">{{ trans('frontend.empresa') }}</a>
        <a href="#o-que-fazemos">{{ trans('frontend.o-que-fazemos') }}</a>
        <a href="#como-fazemos">{{ trans('frontend.como-fazemos') }}</a>
        <a href="#projetos">{{ trans('frontend.projetos.titulo') }}</a>
        <a href="#midia">{{ trans('frontend.midia') }}</a>
        <a class="active">{{ trans('frontend.blog') }}</a>
        <a href="#contato">{{ trans('frontend.contato.titulo') }}</a>
    </nav>
