@extends('frontend.common.template')

@section('content')

    @include('frontend.common.header', ['fixed' => true])

    <div id="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})" class="slide">
                <div class="table">
                    <div class="table-cell">
                        <div class="texto">
                            <h2>{!! $banner->{'titulo_'.app()->getLocale()} !!}</h2>
                            <p>{!! $banner->{'texto_'.app()->getLocale()}!!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="cycle-prev">prev</div>
            <div class="cycle-next">next</div>
        </div>
    </div>

    <div class="empresa" id="empresa">
        <div class="center">
            <h2><span>{{ trans('frontend.empresa') }}</span></h2>
            <div class="texto">{!! $empresa->{'texto_'.app()->getLocale()} !!}</div>
        </div>
    </div>

    <div class="o-que-fazemos" id="o-que-fazemos">
        <div class="center">
            <h2><span>{{ trans('frontend.o-que-fazemos') }}</span></h2>
            <div class="texto">{!! $oQueFazemos->{'abertura_'.app()->getLocale()} !!}</div>
            <div class="icones">
                <div>
                    <img src="{{ asset('assets/img/layout/projeto-arquitetonico-icone.png') }}" alt="">
                    <h3>{{ $oQueFazemos->{'titulo_1_'.app()->getLocale()} }}</h3>
                    <p>{{ $oQueFazemos->{'texto_1_'.app()->getLocale()} }}</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/gerenciamento-icone.png') }}" alt="">
                    <h3>{{ $oQueFazemos->{'titulo_2_'.app()->getLocale()} }}</h3>
                    <p>{{ $oQueFazemos->{'texto_2_'.app()->getLocale()} }}</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/execucao-da-obra-icone.png') }}" alt="">
                    <h3>{{ $oQueFazemos->{'titulo_3_'.app()->getLocale()} }}</h3>
                    <p>{{ $oQueFazemos->{'texto_3_'.app()->getLocale()} }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="como-fazemos" id="como-fazemos">
        <div class="center">
            <h2><span>{{ trans('frontend.como-fazemos') }}</span></h2>
            <div class="texto">{!! $comoFazemos->{'abertura_'.app()->getLocale()} !!}</div>
            <div class="colunas">
                @foreach(range(1, 5) as $i)
                <div>
                    <h3>{!! $comoFazemos->{'titulo_'.$i.'_'.app()->getLocale()} !!}</h3>
                    <p>{{ $comoFazemos->{'texto_'.$i.'_'.app()->getLocale()} }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    @if(count($projetos))
    <div class="projetos" id="projetos">
        <div class="center">
            <h2 style="z-index:104"><span>{{ trans('frontend.projetos.titulo') }}</span></h2>
            <div class="projetos-capas">
                @foreach($projetos as $projeto)
                <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="" data-id="{{ $projeto->id }}" data-titulo="{{ $projeto->{'titulo_'.app()->getLocale()} }}" data-descricao="{{ $projeto->{'descricao_'.app()->getLocale()} }}">
                @endforeach
                <a href="#" id="prev" style="z-index:103">prev</a>
                <a href="#" id="next" style="z-index:103">next</a>
            </div>
            <div class="detalhe-1" style="z-index:102"></div>
            <div class="detalhe-2" style="z-index:102"></div>
            <div class="projetos-descricao" style="z-index:103">
                <img src="{{ asset('assets/img/layout/projeto-icone.png') }}" alt="">
                <h3>{{ $projetos->first()->{'titulo_'.app()->getLocale()} }}</h3>
                <p>{{ $projetos->first()->{'descricao_'.app()->getLocale()} }}</p>
                <a href="#" data-id="{{ $projetos->first()->id }}">{{ trans('frontend.projetos.saiba-mais') }}</a>
            </div>
        </div>

        <div class="projetos-galerias">
            @foreach($projetos as $projeto)
                @foreach($projeto->imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="{{ $projeto->id }}"></a>
                @endforeach
            @endforeach
        </div>
    </div>
    @endif

    @if(count($depoimentos))
    <div class="depoimentos" id="midia">
        <div class="center">
            <h2><span>{{ trans('frontend.midia') }}</span></h2>

            <div class="depoimentos-slick">
                @foreach($depoimentos as $depoimento)
                <div class="depoimento-item">
                    @if($depoimento->video)
                    <div class="video">
                        <iframe width="380" height="270" src="https://www.youtube.com/embed/{{ $depoimento->video }}?rel=0&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    @else
                    <div class="imagem">
                        @if($depoimento->imagem)
                        <img src="{{ asset('assets/img/depoimentos/'.$depoimento->imagem) }}" alt="">
                        @endif
                    </div>
                    @endif

                    <h3>{{ $depoimento->{'titulo_'.app()->getLocale()} }}</h3>
                    <h4>{{ $depoimento->{'subtitulo_'.app()->getLocale()} }}</h4>
                    <p>{!! $depoimento->{'texto_'.app()->getLocale()} !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    @if($blog && count($blog))
    <div class="blog-home" id="espaco-talenco">
        <div class="center">
            <div class="espaco-talenco">
                <span>{!! trans('frontend.blog-destaque') !!}</span>
            </div>

            <div class="posts">
                @foreach($blog as $post)
                <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                    <div class="imagem">
                        <img src="{{ Tools::blogAsset('assets/img/blog/mais/'.$post->capa) }}" alt="">
                        <span>{{ Tools::formataData($post->data) }}</span>
                    </div>

                    <h3>{{ $post->titulo }}</h3>
                    <p>{{ Tools::chamadaPost($post->texto) }}</p>

                    <span class="mais">LER MAIS</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="newsletter-home">
        <div class="center">
            <h2><span>{!! trans('frontend.newsletter.titulo') !!}</span></h2>
            <form action="" id="form-newsletter" method="POST">
                <input type="text" name="newsletter_nome" id="newsletter_nome" placeholder="{{ trans('frontend.newsletter.nome') }}" required>
                <input type="email" name="newsletter_email" id="newsletter_email" placeholder="E-mail" required>
                <input type="submit" value="{{ trans('frontend.newsletter.inscrever') }}">
            </form>
        </div>
    </div>

    <div class="unidades">
        <div class="center">
            <h2><span>{{ trans('frontend.unidades.titulo') }}</span></h2>
            <div class="texto">{!! $unidades->{'texto_'.app()->getLocale()} !!}</div>
            <div class="unidades-lista">
                <div>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/unidades-de-negocio/'.$unidades->imagem_construcao) }}" alt="">
                        <span>
                            ENGENHARIA &<br>CONSTRUÇÃO
                        </span>
                    </div>
                    <div class="logo">
                        <img src="{{ asset('assets/img/layout/marca-talenco-construcao.png') }}" alt="">
                    </div>
                    <div class="texto">{{ $unidades->{'texto_construcao_'.app()->getLocale()} }}</div>
                    <a href="{{ $unidades->link_construcao }}" target="_blank">{{ trans('frontend.unidades.visitar') }}</a>
                </div>
                <div>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/unidades-de-negocio/'.$unidades->imagem_reformas) }}" alt="">
                        <span>
                            SOLUÇÕES EM<br>REFORMAS
                        </span>
                    </div>
                    <div class="logo">
                        <img src="{{ asset('assets/img/layout/marca-talenco-reformas.png') }}" alt="">
                    </div>
                    <div class="texto">{{ $unidades->{'texto_reformas_'.app()->getLocale()} }}</div>
                    <a href="{{ $unidades->link_reformas }}" target="_blank">{{ trans('frontend.unidades.visitar') }}</a>
                </div>
                <div>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/unidades-de-negocio/'.$unidades->imagem_negocios) }}" alt="">
                        <span>
                            NEGÓCIOS<br>IMOBILIÁRIOS
                        </span>
                    </div>
                    <div class="logo">
                        <img src="{{ asset('assets/img/layout/marca-talenco-imobiliarios.png') }}" alt="">
                    </div>
                    <div class="texto">{{ $unidades->{'texto_negocios_'.app()->getLocale()} }}</div>
                    <a href="{{ $unidades->link_negocios }}" target="_blank">{{ trans('frontend.unidades.visitar') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="fale-conosco" id="contato">
        <div class="center">
            <div class="info">
                <h2><span>{{ trans('frontend.contato.fale-conosco') }}</span></h2>
                <p>{{ trans('frontend.contato.frase') }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>
            <div class="icones">
                @foreach(['instagram', 'facebook', 'linkedin', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endif
                @endforeach
                @if($contato->telefone)
                <span class="telefone" data-tipso="{{ $contato->telefone }}">{{ $contato->telefone }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="contato">
        <div class="center">
            <div class="links">
                <span>{{ trans('frontend.contato.institucional') }}</span>
                <a href="#" class="menu-scroll">Home</a>
                <a href="#empresa" class="menu-scroll">{{ trans('frontend.contato.empresa') }}</a>

                <span>{{ trans('frontend.contato.solucoes-reformas') }}</span>
                <a href="#o-que-fazemos" class="menu-scroll">{{ trans('frontend.contato.o-que-fazemos') }}</a>
                <a href="#como-fazemos" class="menu-scroll">{{ trans('frontend.contato.como-fazemos') }}</a>
                <a href="#projetos" class="menu-scroll">{{ trans('frontend.contato.projetos') }}</a>
                <a href="#espaco-talenco" class="menu-scroll">{{ trans('frontend.contato.blog') }}</a>

                <span>{{ trans('frontend.contato.fale-conosco') }}</span>
                <p class="telefone">{{ $contato->telefone }}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            </div>
            <form action="" id="form-contato" method="POST">
                <p>{{ trans('frontend.contato.titulo') }}</p>
                <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.newsletter.nome') }}" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            </form>
        </div>
        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

    @include('frontend.common.copyright')

@endsection
