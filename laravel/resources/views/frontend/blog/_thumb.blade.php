<a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}" class="thumb">
    <div class="imagem">
        <img src="{{ Tools::blogAsset('assets/img/blog/mais/'.$post->capa) }}" alt="">
        <span class="categoria">{{ $post->categoria->titulo }}</span>
    </div>
    <div class="titulo">
        <h5>{{ $post->titulo }}</h5>
        <span class="data">{{ Tools::formataData($post->data) }}</span>
    </div>
    <span class="seta"></span>
</a>
