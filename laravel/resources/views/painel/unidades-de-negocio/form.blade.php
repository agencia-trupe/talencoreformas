@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_construcao', 'Imagem Construção') !!}
            <img src="{{ url('assets/img/unidades-de-negocio/'.$registro->imagem_construcao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_construcao', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_construcao_pt', 'Texto Construção PT') !!}
            {!! Form::textarea('texto_construcao_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_construcao_en', 'Texto Construção EN') !!}
            {!! Form::textarea('texto_construcao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('link_construcao', 'Link Construção') !!}
            {!! Form::text('link_construcao', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_reformas', 'Imagem Reformas') !!}
            <img src="{{ url('assets/img/unidades-de-negocio/'.$registro->imagem_reformas) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_reformas', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_reformas_pt', 'Texto Reformas PT') !!}
            {!! Form::textarea('texto_reformas_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_reformas_en', 'Texto Reformas EN') !!}
            {!! Form::textarea('texto_reformas_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_reformas', 'Link Reformas') !!}
            {!! Form::text('link_reformas', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_negocios', 'Imagem Negócios') !!}
            <img src="{{ url('assets/img/unidades-de-negocio/'.$registro->imagem_negocios) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_negocios', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_negocios_pt', 'Texto Negócios PT') !!}
            {!! Form::textarea('texto_negocios_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_negocios_en', 'Texto Negócios EN') !!}
            {!! Form::textarea('texto_negocios_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_negocios', 'Link Negócios') !!}
            {!! Form::text('link_negocios', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
