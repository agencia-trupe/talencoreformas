@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Unidades de Negócio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.unidades-de-negocio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.unidades-de-negocio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
