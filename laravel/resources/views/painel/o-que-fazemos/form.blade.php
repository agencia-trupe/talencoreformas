@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('abertura_pt', 'Abertura PT') !!}
            {!! Form::textarea('abertura_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('abertura_en', 'Abertura EN') !!}
            {!! Form::textarea('abertura_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_1_pt', 'Título 1 PT') !!}
            {!! Form::text('titulo_1_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_1_en', 'Título 1 EN') !!}
            {!! Form::text('titulo_1_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_2_pt', 'Título 2 PT') !!}
            {!! Form::text('titulo_2_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_2_en', 'Título 2 EN') !!}
            {!! Form::text('titulo_2_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_3_pt', 'Título 3 PT') !!}
            {!! Form::text('titulo_3_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_3_en', 'Título 3 EN') !!}
            {!! Form::text('titulo_3_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_pt', 'Texto 3 PT') !!}
            {!! Form::textarea('texto_3_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_en', 'Texto 3 EN') !!}
            {!! Form::textarea('texto_3_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
