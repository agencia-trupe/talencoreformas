@extends('painel.common.template')

@section('content')

    <legend>
        <h2>O Que Fazemos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.o-que-fazemos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.o-que-fazemos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
