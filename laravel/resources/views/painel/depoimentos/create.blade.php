@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mídia /</small> Adicionar Mídia</h2>
    </legend>

    {!! Form::open(['route' => 'painel.depoimentos.store', 'files' => true]) !!}

        @include('painel.depoimentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
