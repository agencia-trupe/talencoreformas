@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('subtitulo_pt', 'Subtítulo PT') !!}
            {!! Form::text('subtitulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('subtitulo_en', 'Subtítulo EN') !!}
            {!! Form::text('subtitulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="alert alert-info" style="padding:10px 15px;">
    <small>
        <span class="glyphicon glyphicon-info-sign" style="margin-right: 10px;"></span>
        Preencha apenas um dos campos abaixo. Se os dois estiverem preenchidos somente o vídeo aparecerá no site.
    </small>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <img src="{{ url('assets/img/depoimentos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Código do Vídeo no YouTube') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.depoimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
