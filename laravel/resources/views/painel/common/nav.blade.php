<ul class="nav navbar-nav">
	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
	<li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.empresa.index') }}">Empresa</a>
	</li>
	<li @if(str_is('painel.o-que-fazemos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.o-que-fazemos.index') }}">O Que Fazemos</a>
	</li>
	<li @if(str_is('painel.como-fazemos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.como-fazemos.index') }}">Como Fazemos</a>
	</li>
	<li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.projetos.index') }}">Projetos</a>
    </li>
    <li @if(str_is('painel.depoimentos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.depoimentos.index') }}">Mídia</a>
	</li>
	<li @if(str_is('painel.unidades-de-negocio*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.unidades-de-negocio.index') }}">Unidades de Negócio</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName()) || str_is('painel.newsletter*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
</ul>
