@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Como Fazemos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.como-fazemos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.como-fazemos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
