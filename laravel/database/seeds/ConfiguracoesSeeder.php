<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'nome_do_site'               => 'TalenCo Reformas',
            'title'                      => 'TalenCo &middot; Soluções em Reformas',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics'                  => '',
        ]);
    }
}
