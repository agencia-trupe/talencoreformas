<?php

use Illuminate\Database\Seeder;

class OQueFazemosSeeder extends Seeder
{
    public function run()
    {
        DB::table('o_que_fazemos')->insert([
            'abertura_pt' => '',
            'abertura_en' => '',
            'titulo_1_pt' => '',
            'titulo_1_en' => '',
            'texto_1_pt' => '',
            'texto_1_en' => '',
            'titulo_2_pt' => '',
            'titulo_2_en' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'titulo_3_pt' => '',
            'titulo_3_en' => '',
            'texto_3_pt' => '',
            'texto_3_en' => '',
        ]);
    }
}
