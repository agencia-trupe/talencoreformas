<?php

use Illuminate\Database\Seeder;

class UnidadesDeNegocioSeeder extends Seeder
{
    public function run()
    {
        DB::table('unidades_de_negocio')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'imagem_construcao' => '',
            'texto_construcao_pt' => '',
            'texto_construcao_en' => '',
            'link_construcao' => '',
            'imagem_reformas' => '',
            'texto_reformas_pt' => '',
            'texto_reformas_en' => '',
            'link_reformas' => '',
            'imagem_negocios' => '',
            'texto_negocios_pt' => '',
            'texto_negocios_en' => '',
            'link_negocios' => '',
        ]);
    }
}
