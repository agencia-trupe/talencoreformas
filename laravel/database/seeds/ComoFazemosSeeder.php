<?php

use Illuminate\Database\Seeder;

class ComoFazemosSeeder extends Seeder
{
    public function run()
    {
        DB::table('como_fazemos')->insert([
            'abertura_pt' => '',
            'abertura_en' => '',
            'titulo_1_pt' => '',
            'titulo_1_en' => '',
            'texto_1_pt' => '',
            'texto_1_en' => '',
            'titulo_2_pt' => '',
            'titulo_2_en' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'titulo_3_pt' => '',
            'titulo_3_en' => '',
            'texto_3_pt' => '',
            'texto_3_en' => '',
            'titulo_4_pt' => '',
            'titulo_4_en' => '',
            'texto_4_pt' => '',
            'texto_4_en' => '',
            'titulo_5_pt' => '',
            'titulo_5_en' => '',
            'texto_5_pt' => '',
            'texto_5_en' => '',
        ]);
    }
}
