<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOQueFazemosTable extends Migration
{
    public function up()
    {
        Schema::create('o_que_fazemos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('abertura_pt');
            $table->text('abertura_en');
            $table->string('titulo_1_pt');
            $table->string('titulo_1_en');
            $table->text('texto_1_pt');
            $table->text('texto_1_en');
            $table->string('titulo_2_pt');
            $table->string('titulo_2_en');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->string('titulo_3_pt');
            $table->string('titulo_3_en');
            $table->text('texto_3_pt');
            $table->text('texto_3_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('o_que_fazemos');
    }
}
