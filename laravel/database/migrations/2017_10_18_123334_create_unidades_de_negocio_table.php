<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesDeNegocioTable extends Migration
{
    public function up()
    {
        Schema::create('unidades_de_negocio', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->string('imagem_construcao');
            $table->text('texto_construcao_pt');
            $table->text('texto_construcao_en');
            $table->string('link_construcao');
            $table->string('imagem_reformas');
            $table->text('texto_reformas_pt');
            $table->text('texto_reformas_en');
            $table->string('link_reformas');
            $table->string('imagem_negocios');
            $table->text('texto_negocios_pt');
            $table->text('texto_negocios_en');
            $table->string('link_negocios');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('unidades_de_negocio');
    }
}
