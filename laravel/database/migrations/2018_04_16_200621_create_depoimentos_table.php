<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosTable extends Migration
{
    public function up()
    {
        Schema::create('depoimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('subtitulo_pt');
            $table->string('subtitulo_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->string('imagem');
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('depoimentos');
    }
}
