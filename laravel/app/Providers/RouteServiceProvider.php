<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('banners', 'App\Models\Banner');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('o-que-fazemos', 'App\Models\OQueFazemos');
		$router->model('como-fazemos', 'App\Models\ComoFazemos');
		$router->model('projetos', 'App\Models\Projeto');
		$router->model('imagens_projetos', 'App\Models\ProjetoImagem');
		$router->model('unidades-de-negocio', 'App\Models\UnidadesDeNegocio');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($value) {
            return \App\Models\PostCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('post_slug', function($value) {
            return \App\Models\Post::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
