<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class OQueFazemos extends Model
{
    protected $table = 'o_que_fazemos';

    protected $guarded = ['id'];

}
