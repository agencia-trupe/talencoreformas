<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ComoFazemos extends Model
{
    protected $table = 'como_fazemos';

    protected $guarded = ['id'];

}
