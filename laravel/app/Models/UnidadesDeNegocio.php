<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class UnidadesDeNegocio extends Model
{
    protected $table = 'unidades_de_negocio';

    protected $guarded = ['id'];

    public static function upload_imagem_construcao()
    {
        return CropImage::make('imagem_construcao', [
            'width'  => 300,
            'height' => 245,
            'path'   => 'assets/img/unidades-de-negocio/'
        ]);
    }

    public static function upload_imagem_reformas()
    {
        return CropImage::make('imagem_reformas', [
            'width'  => 300,
            'height' => 245,
            'path'   => 'assets/img/unidades-de-negocio/'
        ]);
    }

    public static function upload_imagem_negocios()
    {
        return CropImage::make('imagem_negocios', [
            'width'  => 300,
            'height' => 245,
            'path'   => 'assets/img/unidades-de-negocio/'
        ]);
    }

}
