<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Carbon\Carbon;

class Post extends Model
{
    protected $connection = 'blog';
    protected $table = 'blog';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('blog_categoria_id', $categoria_id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('texto', 'LIKE', "%{$termo}%");
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\PostCategoria', 'blog_categoria_id');
    }
}
