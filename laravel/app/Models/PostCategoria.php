<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class PostCategoria extends Model
{
    protected $connection = 'blog';
    protected $table = 'blog_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function blog()
    {
        return $this->hasMany('App\Models\Post', 'blog_categoria_id')->ordenados();
    }
}
