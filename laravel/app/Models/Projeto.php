<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Projeto extends Model
{
    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoImagem', 'projeto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 960,
            'height' => 550,
            'path'   => 'assets/img/projetos/'
        ]);
    }
}
