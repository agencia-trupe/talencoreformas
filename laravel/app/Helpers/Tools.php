<?php

namespace App\Helpers;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' ' . substr(strtoupper($meses[(int) $mes - 1]), 0, 3) . ' ' . $ano;
    }

    public static function chamadaPost($texto)
    {
        $input = strip_tags($texto);

        if (strlen($input) > 200) {
            $pos   = strpos($input, ' ', 200);
            $input = substr($input, 0, $pos) . '...';
        }

        return $input;
    }

    public static function blogAsset($asset)
    {
        return 'http://talencoimoveis.dev/'.$asset;
    }
}
