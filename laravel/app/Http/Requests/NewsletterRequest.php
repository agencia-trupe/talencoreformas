<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'  => 'required',
            'email' => 'email|required|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'  => trans('frontend.newsletter.nome-required'),
            'email.required' => trans('frontend.newsletter.email-required'),
            'email.email'    => trans('frontend.newsletter.email-email'),
            'email.unique'   => trans('frontend.newsletter.email-unique'),
        ];
    }
}
