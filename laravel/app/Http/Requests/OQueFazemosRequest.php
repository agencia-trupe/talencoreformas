<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OQueFazemosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'abertura_pt' => 'required',
            'abertura_en' => 'required',
            'titulo_1_pt' => 'required',
            'titulo_1_en' => 'required',
            'texto_1_pt' => 'required',
            'texto_1_en' => 'required',
            'titulo_2_pt' => 'required',
            'titulo_2_en' => 'required',
            'texto_2_pt' => 'required',
            'texto_2_en' => 'required',
            'titulo_3_pt' => 'required',
            'titulo_3_en' => 'required',
            'texto_3_pt' => 'required',
            'texto_3_en' => 'required',
        ];
    }
}
