<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'imagem' => 'image',
            'video' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
