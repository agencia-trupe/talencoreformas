<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UnidadesDeNegocioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'imagem_construcao' => 'image',
            'texto_construcao_pt' => 'required',
            'texto_construcao_en' => 'required',
            'link_construcao' => 'required',
            'imagem_reformas' => 'image',
            'texto_reformas_pt' => 'required',
            'texto_reformas_en' => 'required',
            'link_reformas' => 'required',
            'imagem_negocios' => 'image',
            'texto_negocios_pt' => 'required',
            'texto_negocios_en' => 'required',
            'link_negocios' => 'required',
        ];
    }
}
