<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa' => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'descricao_pt' => 'required',
            'descricao_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
