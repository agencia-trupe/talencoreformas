<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Empresa;
use App\Models\OQueFazemos;
use App\Models\ComoFazemos;
use App\Models\Projeto;
use App\Models\Depoimento;
use App\Models\Post;
use App\Models\Newsletter;
use App\Http\Requests\NewsletterRequest;
use App\Models\UnidadesDeNegocio;
use App\Models\Contato;
use App\Models\ContatoRecebido;

class HomeController extends Controller
{
    public function index()
    {
        $banners     = Banner::ordenados()->get();
        $empresa     = Empresa::first();
        $oQueFazemos = OQueFazemos::first();
        $comoFazemos = ComoFazemos::first();
        $projetos    = Projeto::ordenados()->get();
        $depoimentos = Depoimento::ordenados()->get();
        $blog        = Post::ordenados()->take(3)->get();
        $unidades    = UnidadesDeNegocio::first();
        $contato     = Contato::first();

        return view('frontend.home', compact('banners', 'empresa', 'oQueFazemos', 'comoFazemos', 'projetos', 'depoimentos', 'blog', 'unidades', 'contato'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.newsletter.sucesso')
        ];

        return response()->json($response);
    }

    public function contato(ContatoRecebido $contatoRecebido, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'error',
                'message' => trans('frontend.contato.erro')
            ]);
        }

        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'TalenCo Reformas')
                        ->subject('[CONTATO] '.'TalenCo Reformas')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
