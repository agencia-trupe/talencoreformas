<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComoFazemosRequest;
use App\Http\Controllers\Controller;

use App\Models\ComoFazemos;

class ComoFazemosController extends Controller
{
    public function index()
    {
        $registro = ComoFazemos::first();

        return view('painel.como-fazemos.edit', compact('registro'));
    }

    public function update(ComoFazemosRequest $request, ComoFazemos $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.como-fazemos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
