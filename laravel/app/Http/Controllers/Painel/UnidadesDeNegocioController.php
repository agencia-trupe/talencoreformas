<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\UnidadesDeNegocioRequest;
use App\Http\Controllers\Controller;

use App\Models\UnidadesDeNegocio;

class UnidadesDeNegocioController extends Controller
{
    public function index()
    {
        $registro = UnidadesDeNegocio::first();

        return view('painel.unidades-de-negocio.edit', compact('registro'));
    }

    public function update(UnidadesDeNegocioRequest $request, UnidadesDeNegocio $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_construcao'])) $input['imagem_construcao'] = UnidadesDeNegocio::upload_imagem_construcao();
            if (isset($input['imagem_reformas'])) $input['imagem_reformas'] = UnidadesDeNegocio::upload_imagem_reformas();
            if (isset($input['imagem_negocios'])) $input['imagem_negocios'] = UnidadesDeNegocio::upload_imagem_negocios();

            $registro->update($input);

            return redirect()->route('painel.unidades-de-negocio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
