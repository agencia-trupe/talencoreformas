<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OQueFazemosRequest;
use App\Http\Controllers\Controller;

use App\Models\OQueFazemos;

class OQueFazemosController extends Controller
{
    public function index()
    {
        $registro = OQueFazemos::first();

        return view('painel.o-que-fazemos.edit', compact('registro'));
    }

    public function update(OQueFazemosRequest $request, OQueFazemos $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.o-que-fazemos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
