-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: talencoreformas
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'pattern-abstract-honeycomb-metal_20171020132010.jpg','<strong>CUIDAMOS DA SUA OBRA</strong><br />\r\nDO COME&Ccedil;O AO FIM','<strong>CUIDAMOS DA SUA OBRA</strong><br />\r\nDO COME&Ccedil;O AO FIM','Somos uma empresa orientada &agrave; processos e focada<br />\r\nnas &aacute;reas de arquitetura, gerenciamento e engenharia.','Somos uma empresa orientada &agrave; processos e focada<br />\r\nnas &aacute;reas de arquitetura, gerenciamento e engenharia.&nbsp;','2017-10-20 13:20:11','2017-10-20 13:20:25'),(2,2,'pexels-photo-69040_20171020132130.png','<strong>NULLA IN ORCI TURPIS</strong><br />\r\nPELLENTESQUE HABITANT','<strong>NULLA IN ORCI TURPIS</strong><br />\r\nPELLENTESQUE HABITANT','Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />\r\nDuis eleifend purus in justo fermentum interdum.&nbsp;','Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />\r\nDuis eleifend purus in justo fermentum interdum.&nbsp;','2017-10-20 13:21:31','2017-10-20 13:21:31');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `como_fazemos`
--

DROP TABLE IF EXISTS `como_fazemos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `como_fazemos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `abertura_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `abertura_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_4_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_4_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_5_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_5_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_5_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_5_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `como_fazemos`
--

LOCK TABLES `como_fazemos` WRITE;
/*!40000 ALTER TABLE `como_fazemos` DISABLE KEYS */;
INSERT INTO `como_fazemos` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.</p>\r\n','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.</p>\r\n','PLANEJAMENTO E CONTROLE','PLANEJAMENTO E CONTROLE','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','SUPORTE TÉCNICO ESPECIALIZADO','SUPORTE TÉCNICO ESPECIALIZADO','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','GARANTIA DE QUALIDADE','GARANTIA DE QUALIDADE','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','GESTÃO DE SUPRIMENTOS','GESTÃO DE SUPRIMENTOS','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','ACOMPANHAMENTO DA REFORMA ONLINE','ACOMPANHAMENTO DA REFORMA ONLINE','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.',NULL,'2017-10-20 13:25:29');
/*!40000 ALTER TABLE `como_fazemos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_do_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
/*!40000 ALTER TABLE `configuracoes` DISABLE KEYS */;
INSERT INTO `configuracoes` VALUES (1,'TalenCo Reformas','TalenCo &middot; Soluções em Reformas','','','','',NULL,NULL);
/*!40000 ALTER TABLE `configuracoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','+55 13 3395 0005','Rua Esp&iacute;rito Santo, 182 - Canto do Forte<br />\r\nPraia Grande - SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3644.6488435985375!2d-46.409786884509465!3d-24.00817428446296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce1daef198f683%3A0xcbb61a863b38c45c!2sR.+Esp%C3%ADrito+Santo%2C+182+-+Canto+do+Forte%2C+Praia+Grande+-+SP%2C+11700-190!5e0!3m2!1spt-BR!2sbr!4v1508506241903\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','#instagram','#facebook','#linkedin',NULL,'2017-10-20 13:31:42');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum.</p>\r\n\r\n<p>Nunc id dolor massa. Ut rutrum in mi non auctor. Nam commodo erat non turpis euismod mattis.</p>\r\n','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum.</p>\r\n\r\n<p>Nunc id dolor massa. Ut rutrum in mi non auctor. Nam commodo erat non turpis euismod mattis.</p>\r\n',NULL,'2017-10-20 13:22:29');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2017_09_01_163723_create_configuracoes_table',1),('2017_10_18_114214_create_newsletter_table',1),('2017_10_18_123334_create_unidades_de_negocio_table',1),('2017_10_18_123914_create_projetos_table',1),('2017_10_18_130204_create_como_fazemos_table',1),('2017_10_18_130644_create_o_que_fazemos_table',1),('2017_10_18_130932_create_empresa_table',1),('2017_10_18_131111_create_banners_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `o_que_fazemos`
--

DROP TABLE IF EXISTS `o_que_fazemos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `o_que_fazemos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `abertura_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `abertura_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `o_que_fazemos`
--

LOCK TABLES `o_que_fazemos` WRITE;
/*!40000 ALTER TABLE `o_que_fazemos` DISABLE KEYS */;
INSERT INTO `o_que_fazemos` VALUES (1,'<p>Nulla in orci turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\r\n\r\n<p>Suspendisse lobortis sem vel dignissim convallis. Donec at dapibus odio, id finibus ante.</p>\r\n','<p>Nulla in orci turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\r\n\r\n<p>Suspendisse lobortis sem vel dignissim convallis. Donec at dapibus odio, id finibus ante.</p>\r\n','PROJETO ARQUITETÔNICO','PROJETO ARQUITETÔNICO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.','GERENCIAMENTO','GERENCIAMENTO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.','EXECUÇÃO DA OBRA','EXECUÇÃO DA OBRA','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend purus in justo fermentum interdum. Nunc id dolor massa. Ut rutrum in mi non auctor.',NULL,'2017-10-20 13:23:24');
/*!40000 ALTER TABLE `o_que_fazemos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,0,'pexels-photo_20171020132611.jpg','EXEMPLO','EXEMPLO','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','Maecenas et eros sapien. Nulla facilisi. Praesent semper rhoncus sollicitudin.','2017-10-20 13:26:11','2017-10-20 13:26:11'),(2,0,'pattern-abstract-honeycomb-metal_20171020132636.jpg','NOME DO PROJETO','NOME DO PROJETO','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at.','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at.','2017-10-20 13:26:36','2017-10-20 13:26:36');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,2,1,'pattern-abstract-honeycomb-metal_20171020132702.jpg','2017-10-20 13:27:02','2017-10-20 13:27:02'),(2,2,2,'pexels-photo-24578_20171020132707.jpg','2017-10-20 13:27:08','2017-10-20 13:27:08'),(3,1,1,'blue-abstract-glass-balls_20171020132719.jpg','2017-10-20 13:27:19','2017-10-20 13:27:19'),(4,1,2,'pexels-photo-129743_20171020132723.jpeg','2017-10-20 13:27:23','2017-10-20 13:27:23'),(5,1,0,'pexels-photo_20171020132746.jpg','2017-10-20 13:27:47','2017-10-20 13:27:47');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades_de_negocio`
--

DROP TABLE IF EXISTS `unidades_de_negocio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades_de_negocio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_construcao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_construcao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_construcao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `link_construcao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_reformas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_reformas_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_reformas_en` text COLLATE utf8_unicode_ci NOT NULL,
  `link_reformas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_negocios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_negocios_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_negocios_en` text COLLATE utf8_unicode_ci NOT NULL,
  `link_negocios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades_de_negocio`
--

LOCK TABLES `unidades_de_negocio` WRITE;
/*!40000 ALTER TABLE `unidades_de_negocio` DISABLE KEYS */;
INSERT INTO `unidades_de_negocio` VALUES (1,'<p>Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at</p>\r\n','<p>Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at</p>\r\n','pexels-photo-1_20171020132851.jpg','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','#construcao','pexels-photo-69040_20171020132852.png','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','#reformas','pexels-photo-128639_20171020132852.jpeg','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','Donec iaculis varius nisi, sed ultrices purus euismod vitae. Nam mi magna, consequat sit amet ligula at','#negocios',NULL,'2017-10-20 13:28:52');
/*!40000 ALTER TABLE `unidades_de_negocio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$/Ii23Dhq6EndQaXa9qc5MOA65bcZD7/hGkf/145hmKalfQ5dGaB4G','ugFnFSgofDdPdXs2Ykh0afVHsYYPRXDvIAxYsWw9D8UFwdEITmPTadFOKJ8G',NULL,'2017-10-20 13:12:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 15:44:34
